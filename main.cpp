#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"
#include "imgui.h"
#include <GL/gl.h>
#include <GLFW/glfw3.h>
#include "appStyles.hpp"
#include <thread>
#include "gui.hpp"

void updateApp(App& app, bool& shouldClose)
{
	if (!shouldClose)
	{
		app.UpdatePeriodically(shouldClose);
	}
}

int main(void)
{
	App app{.processes = PW::getProcesses()};

	double wWidth = 400, wHeight = 600;

	GUI::GUI gui;

	// Initialize GUI
	if (gui.Init())
	{
		return 1;
	}

	// InitImGuiStyle();
	SetupLightTheme();

	bool shouldClose = false;

	// run app.Update() periodically on a different thread
	std::thread updateThread(
		[&app, &shouldClose]()
		{
			updateApp(app, shouldClose);
		});

	gui.Draw(app, shouldClose);
	updateThread.join();

	// Cleanup
	gui.Clear();

	return 0;
}
