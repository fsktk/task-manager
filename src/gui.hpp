#ifndef GUI_GUI_HPP
#define GUI_GUI_HPP

#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"
#include "imgui.h"
#include <GLFW/glfw3.h>
#include "app.hpp"

namespace GUI
{

struct GUI
{
	GLFWwindow* window;

	int Init()
	{
		if (!glfwInit())
		{
			return 1;
		}
		window = glfwCreateWindow(800, 600, "Task Manager", NULL, NULL);
		if (window == NULL)
		{
			return 1;
		}
		glfwMakeContextCurrent(window);
		glfwSwapInterval(1); // Enable vsync
		// Initialize Dear ImGui
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO();
		(void)io;
		ImGui_ImplGlfw_InitForOpenGL(window, true);
		ImGui_ImplOpenGL3_Init("#version 330");
		return 0;
	}

	void Draw(App& app, bool& shouldClose)
	{
		while (!shouldClose)
		{
			shouldClose = glfwWindowShouldClose(window);

			glfwPollEvents();

			// Start the Dear ImGui frame
			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();

			float bottom = 50.0f; // height of the button pane
			float buttonWidth = 200.0f;
			float buttonHeight = 30.0f;
			float paddingRight = 25.0f;

			int w, h;
			glfwGetWindowSize(window, &w, &h);
			// Set window flags to remove frame decorations and make it full screen
			ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize
				| ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse;
			ImGui::SetNextWindowPos(ImVec2(0, 0));
			ImGui::SetNextWindowSize(ImVec2(w, h));

			// Begin: full-screen ImGui window
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
			ImGui::Begin("Full Screen Window", NULL, window_flags);
			// Update sizes based on user drag input
			// Begin: Process pane
			ImGui::BeginChild("ProcessPane", ImVec2(0, h - bottom - 20), true);
			ImGui::Text("Processes");

			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
			if (ImGui::BeginTable("ProcessTable", 3, ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg))
			{
				// Table header
				ImGui::TableSetupColumn("ID");
				ImGui::TableSetupColumn("User");
				ImGui::TableSetupColumn("Command");
				ImGui::TableHeadersRow();

				for (int i = 0; i < app.processes.size(); i++)
				{
					ImGui::TableNextRow();
					// Check if the process is selected
					// Columns
					// PID
					ImGui::TableSetColumnIndex(0);
					if (ImGui::Selectable(std::to_string(app.processes[i].m_id).c_str(), app.selectedProcessIndex == i, ImGuiSelectableFlags_SpanAllColumns))
					{
						app.selectedProcessIndex = i;
						app.selectedProcessID = app.processes[i].m_id;
					}

					// User
					ImGui::TableSetColumnIndex(1);
					ImGui::Text("%s", app.processes[i].m_user.c_str());
					// Command
					ImGui::TableSetColumnIndex(2);
					ImGui::Text("%s", app.processes[i].m_command.c_str());
				}

				ImGui::EndTable();
				ImGui::PopStyleVar();
			}

			ImGui::EndChild();

			// Begin: Button pane
			ImGui::BeginChild("ButtonPane", ImVec2(0, bottom), true);

			bool enableEndProcessButton = app.selectedProcessIndex >= 0 && app.selectedProcessIndex < app.processes.size();

			if (!enableEndProcessButton)
			{
				ImGui::BeginDisabled();
			}

			ImGui::SameLine(ImGui::GetWindowWidth() - buttonWidth - paddingRight);
			if (ImGui::Button("End Process", ImVec2(buttonWidth, buttonHeight)))
			{
				app.EndProcess();
			}

			// End the disabled block
			if (!enableEndProcessButton)
			{
				ImGui::EndDisabled();
			}
			ImGui::EndChild();
			// end

			// End full-screen ImGui window
			ImGui::End();
			ImGui::PopStyleVar();

			// Render the Dear ImGui frame
			ImGui::Render();
			int display_w, display_h;
			glfwGetFramebufferSize(window, &display_w, &display_h);
			glViewport(0, 0, display_w, display_h);
			glClearColor(0.45f, 0.55f, 0.60f, 1.00f);
			glClear(GL_COLOR_BUFFER_BIT);
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

			glfwSwapBuffers(window);
		}
		std::cout << "done\n";
		return;
	}

	void Clear()
	{
		ImGui_ImplOpenGL3_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();

		glfwDestroyWindow(window);
		glfwTerminate();
	}
};
} // namespace GUI

#endif