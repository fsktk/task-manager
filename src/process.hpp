#ifndef PW_PROCESS_HPP
#define PW_PROCESS_HPP

#include <string>

namespace PW {
class Process {
public:
    int m_id = 0;
    // float m_cpuUsage = 0.0f;
    // float m_memUsage = 0.0f;
    std::string m_user = "";
    std::string m_command = "";

public:
    Process();
    
    Process(int id, std::string user, std::string command);
    
    std::string toString();


};

}


#endif