#include <GLFW/glfw3.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include <random>
#include <thread>
#include <chrono>
#include "pw.hpp"

// #define FAKEDATA(X) (X)
#define FAKEDATA(X)

#define DATA(X) (X)
// #define DATA(X)

struct App
{
	std::vector<PW::Process> processes;
	int selectedProcessIndex = -1;
	int selectedProcessID = 0;

	void EndProcess()
	{
		// Remove the selected process from the list
		PW::killProcess(selectedProcessID);
		processes.erase(processes.begin() + selectedProcessIndex);
		selectedProcessIndex = -1;
		std::cout << "process " << selectedProcessID << " is killed.\n";
		selectedProcessID = 0;
	}

	bool ShouldClose(GLFWwindow* window)
	{
		return glfwWindowShouldClose(window);
	}

	void UpdatePeriodically(bool& shouldClose)
	{
		while (!shouldClose)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(3000));
			Update();
		}
	}

	void Update()
	{
		std::cout << "update\n";

		if (selectedProcessID)
		{
			FAKEDATA(processes = createRandomProcesses(10));
			DATA(processes = PW::getProcesses());

			int pid = selectedProcessID;

			auto it = std::find_if(
				processes.begin(),
				processes.end(),
				[pid, this](const PW::Process& p)
				{
					return p.m_id == selectedProcessID;
				});

			if (it != processes.end())
			{
				selectedProcessIndex = std::distance(processes.begin(), it);
			}
			else
			{
				selectedProcessIndex = -1; // Remove selection
			}
		}
		else
		{
			FAKEDATA(processes = createRandomProcesses(10));
			DATA(processes = PW::getProcesses());

			selectedProcessIndex = -1;
		}
	}

	std::vector<PW::Process> CreateRandomProcesses(int maxProcesses)
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> pidDist(1, 10000);
		std::uniform_int_distribution<> userDist(0, 9);
		std::uniform_int_distribution<> commandDist(0, 19);

		std::vector<std::string> users = {"user1", "user2", "user3", "user4", "user5", "user6", "user7", "user8", "user9", "user10"};
		std::vector<std::string> commands = {"process1",  "process2",  "process3",	"process4",	 "process5",
											 "process6",  "process7",  "process8",	"process9",	 "process10",
											 "process11", "process12", "process13", "process14", "process15",
											 "process16", "process17", "process18", "process19", "process20"};

		int numProcesses = std::uniform_int_distribution<>(1, maxProcesses)(gen);
		std::vector<PW::Process> processes;
		processes.reserve(numProcesses);

		for (int i = 0; i < numProcesses; ++i)
		{
			processes.emplace_back(pidDist(gen), users[userDist(gen)], commands[commandDist(gen)]);
		}

		std::uniform_int_distribution<> dist(0, numProcesses);

		int index = dist(gen);
		processes.insert(processes.begin() + index, {99999, "test-user", "test_command"});
		// processes.emplace_back(99999, "test-user", "test_command");

		return processes;
	}
};