
#ifndef PW_PW_HPP
#define PW_PW_HPP

#include <vector>
#include <array>
#include "process.hpp"
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>
#include <string>



namespace PW {

#include <array>
#include <string>

enum ProcStatusField : int{
    PID = 0,
    Comm,
    State,
    Ppid,
    Pgrp,
    Session,
    Tty_nr,
    Tpgid,
    Flags,
    Minflt,
    Cminflt,
    Majflt,
    Cmajflt,
    Utime,
    Stime,
    Cutime,
    Cstime,
    Priority,
    Nice,
    Num_threads,
    Starttime,
    Vsize,
    Rss,
    Rsslim,
    Startcode,
    Endcode,
    Startstack,
    Kstkesp,
    Kstkeip,
    Signal,
    Blocked,
    Sigignore,
    Sigcatch,
    Wchan,
    Nswap,
    Cnswap,
    Exit_signal,
    Processor,
    RT_Priority,
    Policy,
    Delayacct_blkio_ticks,
    Guest_time,
    Cguest_time,
    Start_data,
    End_data,
    Start_brk,
    Arg_start,
    Arg_end,
    Env_start,
    Env_end,
    Exit_code
};

int getUidFromProcStatus(pid_t pid);

// Write the content of a file into a string
std::string getFileContent(const std::string& path);

// Parse the /proc/[PID]/stat file returns a map (Key: stat file entries)
std::array<std::string, 51> statParser(const std::string& path);

bool isFileAProcess(const char* filename);

// create a vector of running processes
std::vector<PW::Process> getProcesses();

void killProcess(int pid);


}


#endif
