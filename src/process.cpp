#include "process.hpp"

namespace PW {

Process::Process(){}
    
Process::Process(int id, std::string user, std::string command)
    : m_id(id), m_user(user), m_command(command)
{}

std::string Process::toString()
{
    return "PID: " + std::to_string(m_id) + "\t\t" +"User: " + m_user + "\t\tCommand: " + m_command;
}

}
